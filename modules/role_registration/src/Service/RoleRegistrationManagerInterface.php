<?php

namespace Drupal\role_registration\Service;

use Drupal\Core\Form\FormStateInterface;

/**
 * Base interface definition for RoleRegistrationManagerInterface service.
 */
interface RoleRegistrationManagerInterface {

  /**
   * The module name.
   */
  const MODULE_NAME = 'role_registration';

  /**
   * Inject user role in the creation process.
   *
   * @param array $form
   *   Register form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public static function addRoleToUser(array &$form, FormStateInterface $form_state);

  /**
   * Wrapper to check if alias exist.
   *
   * @param string $alias
   *   Alias to check.
   *
   * @return bool
   *   True if alias exist, otherwise FALSE.
   */
  public function isAliasExist(string $alias);

  /**
   * Update alias.
   *
   * @param string $source
   *   Source path.
   * @param string $alias
   *   Alias path.
   */
  public function updateAlias(string $source, string $alias);

  /**
   * Delete alias by source.
   *
   * @param string $source
   *   Source path.
   */
  public function deleteAliasBySource(string $source);

  /**
   * Clean aliases.
   *
   * @param string $alias
   *   Alias.
   *
   * @return string
   *   Processed alias.
   */
  public function cleanAlias(string $alias);

  /**
   * Get default language id.
   */
  public function getDefaultLanguageId();

  /**
   * Get base registration URL.
   *
   * @return string
   *   Registration string URL.
   */
  public function getBaseUrlString(string $role_id);

}
