<?php

namespace Drupal\role_registration\Service;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\path_alias\AliasRepositoryInterface;

/**
 * Class RoleRegistrationManager.
 */
class RoleRegistrationManager implements RoleRegistrationManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Path alias repository.
   *
   * @var \Drupal\path_alias\AliasRepositoryInterface
   */
  protected $aliasRepository;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Alias storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $aliasStorage;

  /**
   * RoleRegistrationManager constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    AliasRepositoryInterface $alias_repository,
    LanguageManagerInterface $language_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->aliasRepository = $alias_repository;
    $this->languageManager = $language_manager;
    $this->aliasStorage = $this->entityTypeManager->getStorage('path_alias');
  }

  /**
   * {@inheritdoc}
   */
  public static function addRoleToUser(array &$form, FormStateInterface $form_state) {
    $role_id = $form_state->getValue('role_id');
    $form_state->setValue(['roles', $role_id], $role_id);
  }

  /**
   * {@inheritdoc}
   */
  public function cleanAlias($alias) {
    // Trim the submitted value of whitespace and slashes. Ensure to not trim
    // the slash on the left side.
    return $alias = rtrim(trim(trim($alias), ''), "\\/");
  }

  /**
   * {@inheritdoc}
   */
  public function isAliasExist(string $alias) {
    $langcode = $this->getDefaultLanguageId();
    return $this->aliasRepository->lookupByAlias($alias, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function updateAlias(string $source, string $alias) {
    // First we check if source has alias.
    $langcode = $this->getDefaultLanguageId();
    $lookupAlias = $this->aliasRepository->lookupBySystemPath($source, $langcode);
    if ($lookupAlias) {
      /** @var \Drupal\path_alias\PathAliasInterface $existing_alias */
      $existing_alias = $this->aliasStorage->load($lookupAlias['id']);
      if ($existing_alias) {
        $existing_alias->set('alias', $alias)->save();
        return $existing_alias->id();
      }
    }
    /** @var \Drupal\path_alias\PathAliasInterface $new_alias */
    $new_alias = $this->aliasStorage->create(['path' => $source, 'alias' => $alias]);
    $new_alias->save();
    return $new_alias->id();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAliasBySource(string $source) {
    $langcode = $this->getDefaultLanguageId();
    $alias = $this->aliasRepository->lookupBySystemPath($source, $langcode);
    if ($alias) {
      $old_alias = $this->aliasStorage->load($alias['id']);
      $old_alias->delete();
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultLanguageId() {
    return $this->languageManager->getDefaultLanguage()->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseUrlString(string $role_id) {
    $url = Url::fromRoute('role_registration.register_page', ['user_role' => $role_id]);
    return '/' . $url->getInternalPath();
  }

}
