<?php

namespace Drupal\Tests\role_registration\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Test register per role.
 *
 * @group role
 */
class RoleRegistrationTest extends BrowserTestBase {

  /**
   * User with admin privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['role', 'role_registration'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer permissions', 'administer users']);
  }

  /**
   * Tests the role registration settings.
   */
  public function testRoleRegistrationSettings() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/people/roles/add');
    $this->assertFieldByXPath('//input[@id="edit-account-registration-status"]', NULL, 'Status field was found');

    $role_name = 'register_role';
    $edit = [
      'label' => $role_name,
      'id' => $role_name,
      'account_registration_status' => 1,
    ];
    $this->drupalPostForm('admin/people/roles/add', $edit, t('Save'));
    $this->assertRaw(t('Role %label has been added.', ['%label' => $role_name]));
    $role = Role::load($role_name);
    $this->assertTrue(is_object($role), 'The role was successfully retrieved from the database.');
  }

  /**
   * Test user registration page.
   */
  public function testRoleRegistrationPage() {
    $role_1 = $this->drupalCreateRole([], 'custom_role_1', 'custom_role_1');
    $role_2 = $this->drupalCreateRole([], 'custom_role_2', 'custom_role_2');
    $role = Role::load($role_1);
    $role->setThirdPartySetting('role_registration', 'account_registration_status', 1)->save();
    $role->setThirdPartySetting('role_registration', 'account_registration_form_mode', 'default')->save();
    $this->drupalGet('user/register/' . $role_1);
    $this->assertResponse(200);
    $this->drupalGet('user/register/' . $role_2);
    $this->assertResponse(404);
  }

}
